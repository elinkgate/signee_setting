package com.elinkgate.signeesettings;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.leanback.preference.LeanbackSettingsFragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragment;
import androidx.preference.PreferenceScreen;

import com.elinkgate.signeesettings.ethernet.EthernetFragment;
import com.elinkgate.signeesettings.ethernet.IpConfigFragment;
import com.elinkgate.signeesettings.ethernet.ProxyConfigFragment;
import com.elinkgate.signeesettings.leds.LedsFragment;
import com.elinkgate.signeesettings.location.LocationFragment;
import com.elinkgate.signeesettings.telephony.TelephonyFragment;
import com.elinkgate.signeesettings.wifi.AccessPointFragment;
import com.elinkgate.signeesettings.wifi.SavedConfigFragment;
import com.elinkgate.signeesettings.wifi.ScanWifiFragment;
import com.elinkgate.signeesettings.wifi.WifiFragment;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.function.Supplier;

import static com.elinkgate.signeesettings.wifi.AccessPointFragment.ACCESS_POINT_KEY_PREFIX;
import static com.elinkgate.signeesettings.wifi.AccessPointFragment.EXTRA_BSSID;
import static com.elinkgate.signeesettings.wifi.AccessPointFragment.EXTRA_NET_ID;
import static com.elinkgate.signeesettings.wifi.AccessPointFragment.EXTRA_SCREEN_TYPE;

public class SettingsFragment extends LeanbackSettingsFragment implements BasePreferenceFragment.PreferenceFragmentListener {

    private static Map<String, Supplier<BasePreferenceFragment>> mFragmentMap = new HashMap<>();
    private Stack<BasePreferenceFragment> mFragmentStack = new Stack<>();

    static {
        mFragmentMap.put("ethernet", EthernetFragment::new);
        mFragmentMap.put("ip_config", IpConfigFragment::new);
        mFragmentMap.put("proxy_config", ProxyConfigFragment::new);
        mFragmentMap.put("wifi", WifiFragment::new);
        mFragmentMap.put("telephony", TelephonyFragment::new);
        mFragmentMap.put("scan_wifi", ScanWifiFragment::new);
        mFragmentMap.put("saved_configs", SavedConfigFragment::new);
        mFragmentMap.put("location", LocationFragment::new);
        mFragmentMap.put("leds", LedsFragment::new);
    }

    public static final class MainPreferenceFragment extends BasePreferenceFragment {

        @Override
        protected int getResourceId() {
            return R.xml.settings_preferences;
        }

        @Override
        protected String getName() {
            return "Main";
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPreferenceStartInitialScreen() {
        MainPreferenceFragment main = new MainPreferenceFragment();
        main.setPreferenceFragmentListener(this);
        startPreferenceFragment(main);
    }

    @Override
    public boolean onPreferenceStartFragment(PreferenceFragment caller, Preference pref) {
        Log.d("DEBUG", "On start preference fragment" + pref.getKey());
        if (pref.getKey().contains(ACCESS_POINT_KEY_PREFIX)) {
            BasePreferenceFragment fragment = new AccessPointFragment();
            if (pref.getPreferenceDataStore() != null) {
                Bundle bundle = new Bundle();
                bundle.putInt(EXTRA_SCREEN_TYPE, pref.getPreferenceDataStore().getInt(EXTRA_SCREEN_TYPE, -1));
                bundle.putString(EXTRA_BSSID, pref.getPreferenceDataStore().getString(EXTRA_BSSID, ""));
                bundle.putInt(EXTRA_NET_ID, pref.getPreferenceDataStore().getInt(EXTRA_NET_ID, -1));
                fragment.setArguments(bundle);
            }
            fragment.setPreferenceFragmentListener(this);
            startPreferenceFragment(fragment);
        } else {
            Supplier<BasePreferenceFragment> supplier = mFragmentMap.get(pref.getKey());
            if (supplier != null) {
                BasePreferenceFragment fragment = supplier.get();
                fragment.setPreferenceFragmentListener(this);
                startPreferenceFragment(fragment);
            }
        }
        return true;
    }

    @Override
    public boolean onPreferenceStartScreen(PreferenceFragment caller, PreferenceScreen pref) {
        return true;
    }

    @Override
    public void onFragmentAttach(BasePreferenceFragment fragment, Context context) {
        mFragmentStack.push(fragment);
        Log.d("DEBUG", "Fragment stack size: " + mFragmentStack.size());
    }

    @Override
    public void onFragmentDetach() {
        if (!mFragmentStack.empty()) {
            mFragmentStack.pop();
        }
        if (!mFragmentStack.empty()) {
            mFragmentStack.peek().onForeground();
        }
        Log.d("DEBUG", "Fragment stack size: " + mFragmentStack.size());
    }
}
