package com.elinkgate.signeesettings;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.elinkgate.signee.SigneeManager;

import leakcanary.LeakCanary;

public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        if (!SigneeManager.isServiceAvailable()) {
            showExitDialog();
            return;
        }

        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            SigneeManager.grantRuntimePermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            SigneeManager.grantRuntimePermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    private void showExitDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Service not found")
                .setMessage("Signee service is not found")
                .setPositiveButton("Close", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                    finish();
                }).create();
        dialog.show();
    }
}
