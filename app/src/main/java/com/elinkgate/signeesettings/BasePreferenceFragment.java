package com.elinkgate.signeesettings;

import android.app.AlertDialog;
import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import androidx.leanback.preference.LeanbackPreferenceFragment;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public abstract class BasePreferenceFragment extends LeanbackPreferenceFragment {

    public static final byte NOT_INTERESTED = 0x00;
    public static final byte WIFI_LISTENER = 0x01;
    public static final byte BLUETOOTH_LISTENER = 0x02;
    public static final byte TELEPHONY_LISTENER = 0x04;
    public static final byte ETHERNET_LISTENER = 0x08;

    private PreferenceFragmentListener mPreferenceFragmentListener;
    private Handler mEventHandler;

    private List<Tracker> mTrackers = new ArrayList<>();

    public BasePreferenceFragment() {
        mEventHandler = new EventHandler(this);
    }

    private static class EventHandler extends Handler {

        WeakReference<BasePreferenceFragment> fragmentWeakRef;

        EventHandler(BasePreferenceFragment fragment) {
            this.fragmentWeakRef = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            if (fragmentWeakRef.get() == null || fragmentWeakRef.get().isDetached()) {
                return;
            }
            fragmentWeakRef.get().onReceivedMessage(msg);
        }
    }

    public void setPreferenceFragmentListener(PreferenceFragmentListener preferenceFragmentListener) {
        this.mPreferenceFragmentListener = preferenceFragmentListener;
    }

    interface PreferenceFragmentListener {

        void onFragmentAttach(BasePreferenceFragment fragment, Context context);

        void onFragmentDetach();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(getResourceId(), rootKey);
    }

    @Override
    public void onAttach(Context context) {
        Log.d("DEBUG", "Attach " + getName() + " fragment");
        if (mPreferenceFragmentListener != null) {
            mPreferenceFragmentListener.onFragmentAttach(this, context);
        }
        WifiConfiguration a;
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        Log.d("DEBUG", "Detach " + getName() + " fragment");
        if (mPreferenceFragmentListener != null) {
            mPreferenceFragmentListener.onFragmentDetach();
        }
        super.onDetach();
    }

    @Override
    public void onStart() {
        Log.d("DEBUG", "Register trackers fragment " + getName());
        for (Tracker tracker : mTrackers) {
            tracker.onStart();
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        Log.d("DEBUG", "Unregister trackers fragment " + getName());
        for (Tracker tracker : mTrackers) {
            tracker.onStop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mTrackers.clear();
        super.onDestroy();
    }

    protected abstract int getResourceId();

    protected abstract String getName();

    protected int getListenerType() {
        return NOT_INTERESTED;
    }

    protected Handler getEventHandler() {
        return mEventHandler;
    }

    protected void onReceivedMessage(Message msg) {
        // Dismiss
    }

    protected void onForeground() {
        Log.d("DEBUG", getName() + " fragment becomes foreground");
        for (Tracker tracker : mTrackers) {
            tracker.requestUpdate();
        }
    }

    protected void showInfoDialog(String title, String msg) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            AlertDialog dialog = new AlertDialog.Builder(getActivity())
                    .setTitle(title)
                    .setMessage(msg)
                    .setPositiveButton("OK", (dialog1, which) -> {
                        dialog1.cancel();
                    }).create();
            dialog.show();
        }
    }

    protected void addTracker(Tracker tracker) {
        tracker.setHandler(mEventHandler);
        mTrackers.add(tracker);
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            Log.d("DEBUG", "Fragment " + getName() + " GC");
        } catch (Exception e) {
            // nothing
        }
        super.finalize();
    }
}
