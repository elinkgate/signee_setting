package com.elinkgate.signeesettings.bluetooth;

import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;

public class BluetoothFragment extends BasePreferenceFragment {

    @Override
    protected int getResourceId() {
        return R.xml.bluetooth_preferences;
    }

    @Override
    protected int getListenerType() {
        return BLUETOOTH_LISTENER;
    }

    @Override
    protected String getName() {
        return "Bluetooth";
    }
}
