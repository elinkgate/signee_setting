package com.elinkgate.signeesettings;

import android.app.Application;

import leakcanary.AppWatcher;
import leakcanary.LeakCanary;

public class SigneeDemoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        AppWatcher.Config wcConf = AppWatcher.getConfig().newBuilder()
                .watchFragmentViews(false)
                .build();

        AppWatcher.setConfig(wcConf);

        LeakCanary.Config lcConf = LeakCanary.getConfig().newBuilder()
                .retainedVisibleThreshold(3)
                .build();

        LeakCanary.setConfig(lcConf);
    }
}
