package com.elinkgate.signeesettings.telephony;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.os.Message;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.SwitchPreference;

import com.elinkgate.signee.SigneeManager;
import com.elinkgate.signee.telephony.MobileNetworkType;
import com.elinkgate.signee.telephony.SupportTelephonyManager;
import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;

import java.util.List;

public class TelephonyFragment extends BasePreferenceFragment {

    private SubscriptionManager mSubscriptionManager;
    private TelephonyManager mTelephonyManager;

    private SupportTelephonyManager mSpTelephonyManager;

    private PreferenceCategory mSubscriptionCategoryPref;
    private SwitchPreference mMobileDataSwitchPref;
    private ListPreference mPreferredNetPref;

    private Preference mMobileDataInfoPref;

    private String mCurNetworkTypeStr = "Unknown";
    private String mCurDownBandwidth = "N/A";
    private String mCurUpBandwidth = "N/A";

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        this.mSubscriptionManager = (SubscriptionManager) getActivity().getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
        this.mTelephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);

        this.mSpTelephonyManager = SigneeManager.getSupportTelephonyManager(getActivity());

        if (mSpTelephonyManager == null) {
            showInfoDialog("Service not found", "GSM service not found");
            getFragmentManager().popBackStack();
            return;
        }

        addTracker(new TelephonyTracker(getActivity()));

        initUi();
    }

    private void initUi() {
        mSubscriptionCategoryPref = (PreferenceCategory) findPreference("subscriptions_category");
        mMobileDataSwitchPref = (SwitchPreference) findPreference("enable_mobile_data");
        mPreferredNetPref = (ListPreference) findPreference("preferred_network");
        mMobileDataInfoPref = (Preference) findPreference("mobile_data_info");

        mPreferredNetPref.setOnPreferenceChangeListener((preference, newValue) -> {
            int netType = Integer.parseInt((String) newValue);
            int subId = SubscriptionManager.getDefaultDataSubscriptionId();
            mSpTelephonyManager.setPreferredNetworkType(subId, netType);
            mPreferredNetPref.setValue((String) newValue);
            mPreferredNetPref.setSummary(mPreferredNetPref.getEntry());
            return true;
        });

        int subId = SubscriptionManager.getDefaultDataSubscriptionId();
        if (subId >= 0) {
            int preferredNetType = mSpTelephonyManager.getPreferredNetworkType(subId);
            mPreferredNetPref.setValue(String.valueOf(preferredNetType));
            mPreferredNetPref.setSummary(getPreferredNetworkTypeName(preferredNetType));
        }
        mMobileDataSwitchPref.setChecked(mTelephonyManager.isDataEnabled());
    }

    @Override
    protected int getListenerType() {
        return TELEPHONY_LISTENER;
    }

    @Override
    protected int getResourceId() {
        return R.xml.telephony_preferences;
    }

    @Override
    protected String getName() {
        return "Telephony";
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        switch (preference.getKey()) {
            case "enable_mobile_data":
                onMobileDataSwitchChanged();
                return false;

        }
        return super.onPreferenceTreeClick(preference);
    }

    private void onMobileDataSwitchChanged() {
        boolean checked = mMobileDataSwitchPref.isChecked();
        mSpTelephonyManager.setDataEnabled(checked);
    }

    @Override
    protected void onReceivedMessage(Message msg) {
        switch (msg.what) {
            case TelephonyTracker.MSG_SUBSCRIPTION_CHANGED:
                onSubscriptionChanged();
                break;
            case TelephonyTracker.MSG_MOBILE_DATA_STATE_CHANGED:
                boolean enabled = mTelephonyManager.isDataEnabled();
                mMobileDataSwitchPref.setChecked(enabled);
                break;
            case TelephonyTracker.MSG_MOBILE_DATA_AVAILABLE:
                mMobileDataSwitchPref.setSummary("Available");
                updatePreferredType();
                break;
            case TelephonyTracker.MSG_MOBILE_DATA_LOST:
            case TelephonyTracker.MSG_MOBILE_DATA_LOSING:
            case TelephonyTracker.MSG_MOBILE_DATA_UNAVAILABLE:
                mMobileDataSwitchPref.setSummary("Unavailable or lost");
            case TelephonyTracker.MSG_MOBILE_DATA_CONNECTION_STATE_CHANGED:
            case TelephonyTracker.MSG_SERVICE_STATE_CHANGED:
                updateNetworkType();
                break;
            case TelephonyTracker.MSG_NET_CAPABILITIES_CHANGED:
                NetworkCapabilities cap = (NetworkCapabilities) msg.obj;
                updateBandwidth(cap.getLinkDownstreamBandwidthKbps(), cap.getLinkUpstreamBandwidthKbps());
                break;
        }
    }

    private void updateBandwidth(int dl, int ul) {
        mCurDownBandwidth = String.format("%-5d Kbps", dl);
        mCurUpBandwidth = String.format("%-5d Kbps", ul);
        updateNetworkInfo();
    }

    private void updateNetworkType() {
        mCurNetworkTypeStr = getCurrentNetworkTypeName(mTelephonyManager.getNetworkType());
        updateNetworkInfo();
    }

    private void updateNetworkInfo() {
        mMobileDataInfoPref.setSummary("Network type: " + mCurNetworkTypeStr +
                "\nDownload: " + mCurDownBandwidth +
                "\nUpload: " + mCurUpBandwidth);
    }

    private void onSubscriptionChanged() {
        if (getActivity() == null || getActivity().isFinishing()) {
            return;
        }
        mSubscriptionCategoryPref.removeAll();
        if (getActivity().checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (SigneeManager.grantRuntimePermission(getActivity(), Manifest.permission.READ_PHONE_STATE)) {
            }
        }
        int i = 0;
        List<SubscriptionInfo> subs = mSubscriptionManager.getActiveSubscriptionInfoList();
        if (subs == null) {
            return;
        }
        for (SubscriptionInfo sub : subs) {
            Preference pref = new Preference(getPreferenceManager().getContext());
            pref.setKey("sim_" + i++);
            pref.setTitle(sub.getDisplayName());
            pref.setSummary(sub.getCarrierName());
            pref.setIcon(R.drawable.ic_sim_card_24dp);
            mSubscriptionCategoryPref.addPreference(pref);
        }
    }

    private void updatePreferredType() {
        String netWorkTypeName = getCurrentNetworkTypeName(mTelephonyManager.getNetworkType());
        mMobileDataSwitchPref.setSummary("Connected (" + netWorkTypeName + ")");
    }

    private String getCurrentNetworkTypeName(int type) {
        switch (type) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "GPRS";
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "EDGE";
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "UMTS";
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "HSDPA";
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "HSUPA";
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "HSPA";
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "CDMA";
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return "CDMA - EvDo rev. 0";
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return "CDMA - EvDo rev. A";
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return "CDMA - EvDo rev. B";
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return "CDMA - 1xRTT";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "LTE";
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return "CDMA - eHRPD";
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "iDEN";
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "HSPA+";
            case TelephonyManager.NETWORK_TYPE_GSM:
                return "GSM";
            case TelephonyManager.NETWORK_TYPE_TD_SCDMA:
                return "TD_SCDMA";
            case TelephonyManager.NETWORK_TYPE_IWLAN:
                return "IWLAN";
            default:
                return "UNKNOWN";
        }
    }

    private String getPreferredNetworkTypeName(@MobileNetworkType int type) {
        switch (type) {
            case MobileNetworkType.NETWORK_MODE_GSM_ONLY:
                return "2G";
            case MobileNetworkType.NETWORK_MODE_WCDMA_PREF:
                return "3G";
            case MobileNetworkType.NETWORK_MODE_LTE_GSM_WCDMA:
                return "4G";
        }
        return "";
    }
}
