package com.elinkgate.signeesettings.telephony;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.elinkgate.signee.SigneeManager;
import com.elinkgate.signee.telephony.MobileDataStateCallback;
import com.elinkgate.signee.telephony.SupportTelephonyManager;
import com.elinkgate.signeesettings.Tracker;
import com.elinkgate.signeesettings.utils.ConnectivityThread;
import com.elinkgate.signeesettings.utils.Protocols;

public class TelephonyTracker extends Tracker {

    public static final int BASE = Protocols.TELEPHONY;

    public static final int MSG_SUBSCRIPTION_CHANGED = BASE + 1;
    public static final int MSG_MOBILE_DATA_STATE_CHANGED = BASE + 2;
    public static final int MSG_MOBILE_DATA_AVAILABLE = BASE + 3;
    public static final int MSG_MOBILE_DATA_UNAVAILABLE = BASE + 4;
    public static final int MSG_MOBILE_DATA_LOST = BASE + 5;
    public static final int MSG_MOBILE_DATA_LOSING = BASE + 6;
    public static final int MSG_MOBILE_DATA_CONNECTION_STATE_CHANGED = BASE + 7;
    public static final int MSG_MOBILE_DATA_SIGNAL_STRENGTHS_CHANGED = BASE + 8;
    public static final int MSG_NET_CAPABILITIES_CHANGED = BASE + 9;
    public static final int MSG_SERVICE_STATE_CHANGED = BASE + 10;

    private SubscriptionManager mSubscriptionManager;

    private TelephonyManager mTelephonyManager;

    private ConnectivityManager mConnectivityManager;

    private SupportTelephonyManager mSpTelephonyManager;

    private static final NetworkRequest mMobileNetworkRequest;

    static {
        mMobileNetworkRequest = new NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                .build();
    }

    public TelephonyTracker(Context context) {
        super(context);
        this.mSubscriptionManager = (SubscriptionManager) context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
        this.mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        this.mTelephonyManager = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE))
                .createForSubscriptionId(SubscriptionManager.getDefaultDataSubscriptionId());
        this.mSpTelephonyManager = SigneeManager.getSupportTelephonyManager(context);
    }

    private final MobileDataStateCallback mMobileDataStateCallback = enabled -> sendMessage(MSG_MOBILE_DATA_STATE_CHANGED, enabled ? 1 : 0);

    private final SubscriptionManager.OnSubscriptionsChangedListener mOnSubscriptionChangedListener =
            new SubscriptionManager.OnSubscriptionsChangedListener() {
                @Override
                public void onSubscriptionsChanged() {
                    sendMessage(MSG_SUBSCRIPTION_CHANGED);
                }
            };

    private final ConnectivityManager.NetworkCallback mMobileDataNetworkCallback = new ConnectivityManager.NetworkCallback() {

        @Override
        public void onLost(Network network) {
            sendMessage(MSG_MOBILE_DATA_LOST);
        }

        @Override
        public void onLosing(Network network, int maxMsToLive) {
            sendMessage(MSG_MOBILE_DATA_LOSING);
        }

        @Override
        public void onAvailable(Network network) {
            sendMessage(MSG_MOBILE_DATA_AVAILABLE);
        }

        @Override
        public void onUnavailable() {
            sendMessage(MSG_MOBILE_DATA_UNAVAILABLE);
        }

        @Override
        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            sendMessage(MSG_NET_CAPABILITIES_CHANGED, 0, networkCapabilities);
        }
    };

    private final PhoneStateListener mPhoneStateListener = new PhoneStateListener() {

        @Override
        public void onDataConnectionStateChanged(int state) {
            sendMessage(MSG_MOBILE_DATA_CONNECTION_STATE_CHANGED);
        }

        @Override
        public void onServiceStateChanged(ServiceState serviceState) {
            sendMessage(MSG_SERVICE_STATE_CHANGED);
        }
    };

    @Override
    public void onStart() {
        mSubscriptionManager.addOnSubscriptionsChangedListener(mOnSubscriptionChangedListener);
        mConnectivityManager.registerNetworkCallback(mMobileNetworkRequest, mMobileDataNetworkCallback,
                new Handler(ConnectivityThread.getThreadLooper()));
        mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_DATA_CONNECTION_STATE
                | PhoneStateListener.LISTEN_SIGNAL_STRENGTHS
                | PhoneStateListener.LISTEN_SERVICE_STATE);
        mSpTelephonyManager.registerMobileDataStateCallback(mMobileDataStateCallback,
                new Handler(ConnectivityThread.getThreadLooper()));
    }

    @Override
    public void onStop() {
        mSubscriptionManager.removeOnSubscriptionsChangedListener(mOnSubscriptionChangedListener);
        mConnectivityManager.unregisterNetworkCallback(mMobileDataNetworkCallback);
        mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
        mSpTelephonyManager.unregisterMobileDataStateCallback(mMobileDataStateCallback);
    }

    @Override
    public void requestUpdate() {

    }
}
