package com.elinkgate.signeesettings.location;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.elinkgate.signeesettings.Tracker;

public class LocationTracker extends Tracker {

    public static final int MSG_LOCATION_CHANGED = 1;
    public static final int MSG_ON_STATUS_CHANGED = 2;
    public static final int MSG_PROVIDER_STATE_CHANGED = 3;

    private LocationManager mLocationManager;

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            sendMessage(MSG_LOCATION_CHANGED, 0, location);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            sendMessage(MSG_ON_STATUS_CHANGED);
        }

        @Override
        public void onProviderEnabled(String s) {
            if (LocationManager.GPS_PROVIDER.equals(s)) {
                sendMessage(MSG_PROVIDER_STATE_CHANGED, 1);
            }
        }

        @Override
        public void onProviderDisabled(String s) {
            if (LocationManager.GPS_PROVIDER.equals(s)) {
                sendMessage(MSG_PROVIDER_STATE_CHANGED, 0);
            }
        }
    };

    public LocationTracker(Context context) {
        super(context);
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onStart() {
        sendMessage(MSG_LOCATION_CHANGED, 0, mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000, 0, mLocationListener);
    }

    @Override
    public void onStop() {
        mLocationManager.removeUpdates(mLocationListener);
    }

    @Override
    public void requestUpdate() {

    }
}
