package com.elinkgate.signeesettings.location;

import android.location.Location;
import android.os.Bundle;
import android.os.Message;

import androidx.preference.Preference;
import androidx.preference.SwitchPreference;

import com.elinkgate.signee.SigneeManager;
import com.elinkgate.signee.location.SupportLocationManager;
import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;

public class LocationFragment extends BasePreferenceFragment {

    private Preference mGpsInfoPref;
    private SwitchPreference mLocationSwitch;
    private SupportLocationManager mSpLocationManager;
    private Location mLocation;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        addTracker(new LocationTracker(getContext()));

        mSpLocationManager = SigneeManager.getSupportLocationManager(getContext());

        mGpsInfoPref = findPreference("gps_info");

        mLocationSwitch = (androidx.preference.SwitchPreference) findPreference("enable_location");
        mLocationSwitch.setChecked(mSpLocationManager.isLocationOn());

        mLocationSwitch.setOnPreferenceChangeListener((preference, newValue) -> {
            boolean enabled = (boolean) newValue;
            mSpLocationManager.setLocationEnabled(enabled);
            return true;
        });

        mGpsInfoPref.setSummary("Latitude: N/A\nLongitude: N/A");
    }

    private void updateGpsInfo() {
        String info = "Latitude: " + mLocation.getLatitude() +
                "\nLongitude:" + mLocation.getLongitude();
        mGpsInfoPref.setSummary(info);
    }

    @Override
    protected int getResourceId() {
        return R.xml.location_preferences;
    }

    @Override
    protected String getName() {
        return "Location";
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    protected void onReceivedMessage(Message msg) {
        switch (msg.what) {
            case LocationTracker.MSG_LOCATION_CHANGED:
                if (msg.obj != null) {
                    mLocation = (Location) msg.obj;
                    updateGpsInfo();
                }
                break;
        }
    }
}
