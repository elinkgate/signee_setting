package com.elinkgate.signeesettings.wifi;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import androidx.preference.PreferenceCategory;

import com.elinkgate.signee.wifi.WifiConfigUtils;
import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;
import com.elinkgate.signeesettings.utils.ActionPreference;
import com.elinkgate.signeesettings.utils.SimplePreferenceDataStore;

import java.util.List;

public class SavedConfigFragment extends BasePreferenceFragment {

    private PreferenceCategory mSavedConfigPref;
    private WifiManager mWifiManager;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        mWifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);

        mSavedConfigPref = (PreferenceCategory) findPreference("saved_config_category");

        updateUi();
    }

    private void updateUi() {
        mSavedConfigPref.removeAll();
        int i = 0;
        List<WifiConfiguration> configs = mWifiManager.getConfiguredNetworks();
        for (WifiConfiguration cf : configs) {
            ActionPreference apPref = new ActionPreference(getPreferenceManager().getContext());

            apPref.setKey(AccessPointFragment.ACCESS_POINT_KEY_PREFIX + i++);
            apPref.setTitle(WifiConfigUtils.removeDoubleQuotes(cf.SSID));
            apPref.setSummary(cf.BSSID);
            apPref.setSelectable(true);
            apPref.setFragment(AccessPointFragment.class.getName());

            SimplePreferenceDataStore dataStore = new SimplePreferenceDataStore();
            dataStore.putInt(AccessPointFragment.EXTRA_SCREEN_TYPE, AccessPointFragment.VIEW_SAVED_CONFIG_SCREEN);
            dataStore.putInt(AccessPointFragment.EXTRA_NET_ID, cf.networkId);
            apPref.setPreferenceDataStore(dataStore);

            mSavedConfigPref.addPreference(apPref);
        }
    }

    @Override
    protected int getResourceId() {
        return R.xml.saved_config_preferences;
    }

    @Override
    protected void onForeground() {
        super.onForeground();
        updateUi();
    }

    @Override
    protected String getName() {
        return "Saved configurations";
    }
}
