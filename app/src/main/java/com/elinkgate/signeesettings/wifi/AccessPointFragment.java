package com.elinkgate.signeesettings.wifi;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;

import com.elinkgate.signee.SigneeManager;
import com.elinkgate.signee.wifi.SupportWifiManager;
import com.elinkgate.signee.wifi.WifiConfigUtils;
import com.elinkgate.signee.wifi.WifiSecurity;
import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;
import com.elinkgate.signeesettings.utils.ActionPreference;
import com.elinkgate.signeesettings.utils.FeaturedEditPreference;

import java.util.List;

public class AccessPointFragment extends BasePreferenceFragment {

    public static final String ACCESS_POINT_KEY_PREFIX = "dynamic_access_point_";

    public static final String EXTRA_BSSID = "extra_bssid";
    public static final String EXTRA_SCREEN_TYPE = "extra_screen_type";
    public static final String EXTRA_NET_ID = "extra_net_id";

    public static final int CONNECT_SCREEN = 0x0001;
    public static final int VIEW_SAVED_CONFIG_SCREEN = 0x0002;
    public static final int CONFIG_ACCESS_POINT = 0x0003;

    private int mScreenType = VIEW_SAVED_CONFIG_SCREEN;

    private EditTextPreference mSsidPreference;
    private ListPreference mSecurityPreference;
    private FeaturedEditPreference mPassPreference;
    private ActionPreference mAcceptButton;
    private ActionPreference mForgetButton;

    private WifiManager mWifiManager;
    private SupportWifiManager mSpWifiManager;

    private String mSsid = "";

    @WifiSecurity
    private int mSecurityType = WifiSecurity.WIFI_SECURITY_UNKNOWN;

    private String mPass = "";
    private int mNetId = -1;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        mWifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        mSpWifiManager = SigneeManager.getSupportWifiManager(getActivity());

        mSsidPreference = (EditTextPreference) findPreference("ssid");
        mSecurityPreference = (ListPreference) findPreference("security_type");
        mPassPreference = (FeaturedEditPreference) findPreference("pass");
        mAcceptButton = (ActionPreference) findPreference("ap_accept");
        mForgetButton = (ActionPreference) findPreference("forget");

        mPassPreference.setHideSummary(true);

        initUi();
    }

    private void initUi() {
        int screenType = getArguments().getInt(EXTRA_SCREEN_TYPE);
        switch (screenType) {
            case CONNECT_SCREEN:
                mScreenType = CONNECT_SCREEN;
                mSsidPreference.setSelectable(false);
                mSecurityPreference.setSelectable(false);
                mPassPreference.setSelectable(true);
                mAcceptButton.setOnLickListener(v -> connect());
                initForConnectScreen();
                break;
            case VIEW_SAVED_CONFIG_SCREEN:
                mScreenType = VIEW_SAVED_CONFIG_SCREEN;
                mSsidPreference.setSelectable(false);
                mSecurityPreference.setSelectable(false);
                mPassPreference.setSelectable(true);
                mPassPreference.setEditTextEnabled(false);
                mPassPreference.setWidgetLayoutResource(R.layout.eye_icon_layout);
                mPassPreference.setOnLickListener(v -> viewPass());
                mAcceptButton.setVisible(false);
                mForgetButton.setVisible(true);
                mForgetButton.setOnLickListener(v -> forget());
                initSavedConfigsScreen();
                break;
            case CONFIG_ACCESS_POINT:
                mScreenType = CONFIG_ACCESS_POINT;
                mSsidPreference.setSelectable(true);
                mSecurityPreference.setSelectable(true);
                mPassPreference.setSelectable(true);
                mAcceptButton.setOnLickListener(v -> close());
                break;
            default:
                close();
        }
    }

    private void initForConnectScreen() {
        String bssid = getArguments().getString(EXTRA_BSSID);
        if (!TextUtils.isEmpty(bssid)) {
            List<ScanResult> results = mWifiManager.getScanResults();
            ScanResult ap = null;
            for (ScanResult sr : results) {
                if (sr.BSSID.equals(bssid)) {
                    ap = sr;
                    break;
                }
            }
            if (ap == null) {
                return;
            }
            mSecurityType = WifiConfigUtils.getSecurityType(ap);
            mSsid = ap.SSID;
            mSsidPreference.setSummary(mSsid);
            selectSecurityType(mSecurityType);
        }
    }

    private void initSavedConfigsScreen() {
        int netId = getArguments().getInt(EXTRA_NET_ID);
        if (netId != -1) {
            List<WifiConfiguration> results = mWifiManager.getConfiguredNetworks();
            WifiConfiguration config = null;
            for (WifiConfiguration cf : results) {
                if (cf.networkId == netId) {
                    config = cf;
                    break;
                }
            }
            if (config == null) {
                return;
            }
            mNetId = config.networkId;
            mSecurityType = WifiConfigUtils.getSecurityType(config);
            mSsid = WifiConfigUtils.removeDoubleQuotes(config.SSID);
            mSsidPreference.setSummary(mSsid);
            selectSecurityType(mSecurityType);
            mPass = mSpWifiManager.getPassphrase(config.networkId);
            mPass = WifiConfigUtils.removeDoubleQuotes(mPass);
            mPassPreference.setText(mPass);
            mPassPreference.setSummary(mPass.replaceAll(".", "*"));
        }
    }

    private void selectSecurityType(@WifiSecurity int type) {
        switch (type) {
            case WifiSecurity.WIFI_SECURITY_PSK:
                mSecurityPreference.setValue("PSK");
                break;
            case WifiSecurity.WIFI_SECURITY_NONE:
                mSecurityPreference.setValue("NONE");
                break;
        }
        mSecurityPreference.setSummary(mSecurityPreference.getValue());
    }

    private void connect() {
        String pass = mPassPreference.getText();
        WifiConfiguration config = new WifiConfiguration();
        if (mSecurityType == WifiSecurity.WIFI_SECURITY_PSK) {
            if (TextUtils.isEmpty(pass)) {
                showInfoDialog("Invalid information", "Please provide an passphrase for WPA-PSK network");
                return;
            }
            WifiConfigUtils.setWpaPreSharedKey(config, pass);
        }
        WifiConfigUtils.setSsid(config, mSsid);
        WifiConfigUtils.setKeyManagement(config, mSecurityType);
        int netId = mWifiManager.addNetwork(config);
        if (netId != -1) {
            mWifiManager.enableNetwork(netId, true);
            mWifiManager.reconnect();
        }
        close();
    }

    private void close() {
        getFragmentManager().popBackStack();
    }

    private void viewPass() {
        showInfoDialog("Passphrase of " + mSsid, mPass);
    }

    private void forget() {
        if (mNetId != -1) {
            mSpWifiManager.forget(mNetId, null);
        }
        close();
    }

    @Override
    protected int getResourceId() {
        return R.xml.access_point_preferences;
    }

    @Override
    protected String getName() {
        return "Access point";
    }
}
