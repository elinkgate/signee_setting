package com.elinkgate.signeesettings.wifi;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.preference.CheckBoxPreference;
import androidx.preference.Preference;
import androidx.preference.SwitchPreference;

import com.elinkgate.signee.ActionCallback;
import com.elinkgate.signee.SigneeManager;
import com.elinkgate.signee.wifi.LocalOnlyHotspotCallback;
import com.elinkgate.signee.wifi.SupportWifiManager;
import com.elinkgate.signee.wifi.WifiConfigUtils;
import com.elinkgate.signee.wifi.WifiSecurity;
import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;
import com.elinkgate.signeesettings.utils.FeaturedEditPreference;
import com.elinkgate.signeesettings.utils.FeaturedListPreference;

public class WifiFragment extends BasePreferenceFragment {

    private SwitchPreference mEnableWifiPref;
    private WifiManager mWifiManager;
    private SupportWifiManager mSupportWifiManager;
    private boolean mWifiChangedByUser = false;

    private SwitchPreference mHotspotSwitchPref;
    private CheckBoxPreference mTetheringPref;
    private FeaturedEditPreference mHotspotNamePref;
    private FeaturedListPreference mSecurityTypePref;
    private FeaturedEditPreference mPassphrasePref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        mSupportWifiManager = SigneeManager.getSupportWifiManager(getActivity());
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        addTracker(new WifiTracker(getContext()));
        initialize();
    }

    private void initialize() {
        mEnableWifiPref = (SwitchPreference) findPreference("enable_wifi");
        mHotspotSwitchPref = (SwitchPreference) findPreference("enable_hotspot");
        mTetheringPref = (CheckBoxPreference) findPreference("tethering");
        mHotspotNamePref = (FeaturedEditPreference) findPreference("hotspot_name");
        mSecurityTypePref = (FeaturedListPreference) findPreference("security_type");
        mPassphrasePref = (FeaturedEditPreference) findPreference("passphrase");

        mSecurityTypePref.setOnPreferenceChangeListener((preference, newValue) -> {
            mSecurityTypePref.setSummary(mSecurityTypePref.getEntry());
            return true;
        });

        mHotspotNamePref.initText("Signee hotspot");
        mSecurityTypePref.initValue("0");
        mPassphrasePref.setHideSummary(true);
        mPassphrasePref.initText("12345678");
    }

    @Override
    protected int getResourceId() {
        return R.xml.wifi_preferences;
    }

    public void setWifiSwitch(boolean enable) {
        if (!mWifiChangedByUser) {
            mEnableWifiPref.setChecked(enable);
        } else {
            mWifiChangedByUser = false;
        }
    }

    public void updateConnectionState(int state) {
        switch (state) {
            case WifiTracker.STATE_WIFI_CONNECTED:
                String ssid = mWifiManager.getConnectionInfo().getSSID();
                mEnableWifiPref.setSummary(WifiConfigUtils.removeDoubleQuotes(ssid));
                break;
            case WifiTracker.STATE_WIFI_CONNECTING:
                mEnableWifiPref.setSummary("Connecting...");
                break;
            case WifiTracker.STATE_WIFI_DISCONNECTED:
                mEnableWifiPref.setSummary("Disconnected");
                break;
            case WifiTracker.STATE_WIFI_ERROR_AUTH:
                mEnableWifiPref.setSummary("Authenticating error");
                break;
        }
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        switch (preference.getKey()) {
            case "enable_wifi":
                mWifiManager.setWifiEnabled(mEnableWifiPref.isChecked());
                mWifiChangedByUser = mEnableWifiPref.isChecked();
                break;
            case "enable_hotspot":
                boolean called = onWifiHotspotEnabled(mHotspotSwitchPref.isChecked());
                mHotspotSwitchPref.setEnabled(!called);
                if (!called) {
                    mHotspotSwitchPref.setChecked(!mHotspotSwitchPref.isChecked());
                }
        }
        return super.onPreferenceTreeClick(preference);
    }

    private boolean onWifiHotspotEnabled(boolean enabled) {
        boolean isTethering = mTetheringPref.isChecked();
        if (enabled) {
            mWifiManager.setWifiEnabled(false);
            WifiConfiguration config = createHotspotConfig();
            if (config == null) {
                showInfoDialog("Invalid", "Please provide an valid configuration");
                return false;
            }
            if (isTethering) {
                if (mSupportWifiManager.setWifiApConfiguration(config)) {
                    mSupportWifiManager.startWifiTethering(new ActionCallback() {
                        @Override
                        public void onSuccess() {
                            Log.d("DEBUG", "tether success");
                        }

                        @Override
                        public void onFailed(int reason) {
                            Log.d("DEBUG", "tether failed");
                        }
                    }, null);
                } else {
                    showInfoDialog("Invalid", "Invalid wifi hotspot configuration");
                    return false;
                }
            } else {
                mSupportWifiManager.startLocalOnlyHotspot(mLocalOnlyHotspotCallback, config, true);
            }
            return true;
        } else {
            if (isTethering) {
                mSupportWifiManager.stopWifiTethering();
            } else {
                mSupportWifiManager.stopLocalOnlyHotspot();
            }
            return true;
        }
    }

    private final LocalOnlyHotspotCallback mLocalOnlyHotspotCallback = new LocalOnlyHotspotCallback() {

        @Override
        public void onStarted(WifiConfiguration wifiConfiguration) {
            Log.d("DEBUG", "onStarted");
            mHotspotSwitchPref.setEnabled(true);
        }

        @Override
        public void onStopped() {
            Log.d("DEBUG", "onStop");
            mHotspotSwitchPref.setEnabled(true);
        }

        @Override
        public void onFailed(int i) {
            if (i == LocalOnlyHotspotCallback.ERROR_LOCATION_OFF) {
                showInfoDialog("Error", "Location is off");
            } else {
                showInfoDialog("Error", "Cannot start local hotspot");
            }
            mHotspotSwitchPref.setEnabled(true);
        }
    };

    @Nullable
    private WifiConfiguration createHotspotConfig() {
        String ssid = mHotspotNamePref.getText();
        String pass = mPassphrasePref.getText();
        if (TextUtils.isEmpty(ssid) || TextUtils.isEmpty(pass)) {
            return null;
        }
        WifiConfiguration config = new WifiConfiguration();
        int secType = getSecurityType();
        WifiConfigUtils.setKeyManagement(config, secType);
        config.SSID = ssid;
        if (secType == WifiSecurity.WIFI_SECURITY_PSK) {
            config.preSharedKey = pass;
            return config;
        } else if (secType == WifiSecurity.WIFI_SECURITY_NONE) {
            return config;
        }
        return null;
    }

    private int getSecurityType() {
        int type = Integer.parseInt(mSecurityTypePref.getValue());
        switch (type) {
            case 0:
                return WifiSecurity.WIFI_SECURITY_PSK;
            case 1:
                return WifiSecurity.WIFI_SECURITY_NONE;
        }
        return WifiSecurity.WIFI_SECURITY_UNKNOWN;
    }

    @Override
    protected void onReceivedMessage(Message msg) {
        switch (msg.what) {
            case WifiTracker.MSG_WIFI_STATE:
                setWifiSwitch(msg.arg1 == 1);
                if (msg.arg1 == 0) {
                    updateConnectionState(WifiTracker.STATE_WIFI_DISCONNECTED);
                }
                break;
            case WifiTracker.MSG_WIFI_CONNECTION:
                updateConnectionState(msg.arg1);
                break;
            case WifiTracker.MSG_WIFI_AP_STATE:
                mHotspotSwitchPref.setEnabled(true);
                mHotspotSwitchPref.setChecked(msg.arg1 == SupportWifiManager.WIFI_AP_STATE_ENABLED);
                setHotspotConfigurationEnbled(!mHotspotSwitchPref.isChecked());
                break;
        }
    }

    private void setHotspotConfigurationEnbled(boolean enabled) {
        mTetheringPref.setEnabled(enabled);
        mHotspotNamePref.setEnabled(enabled);
        mSecurityTypePref.setEnabled(enabled);
        mPassphrasePref.setEnabled(enabled);
    }

    @Override
    protected int getListenerType() {
        return WIFI_LISTENER;
    }

    @Override
    protected String getName() {
        return "Wifi";
    }
}
