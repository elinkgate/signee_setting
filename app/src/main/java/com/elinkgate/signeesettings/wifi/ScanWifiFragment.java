package com.elinkgate.signeesettings.wifi;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Message;

import androidx.preference.PreferenceCategory;

import com.elinkgate.signee.SigneeManager;
import com.elinkgate.signee.wifi.SupportWifiManager;
import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;
import com.elinkgate.signeesettings.utils.ActionPreference;
import com.elinkgate.signeesettings.utils.SimplePreferenceDataStore;

import java.util.List;

public class ScanWifiFragment extends BasePreferenceFragment {

    private WifiManager mWifiManager;
    private SupportWifiManager mSpWifiManager;
    private ActionPreference mRefreshScanBtn;
    private PreferenceCategory mScanResultsCategory;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        addTracker(new WifiTracker(getActivity()));

        mSpWifiManager = SigneeManager.getSupportWifiManager(getActivity());
        mWifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        mScanResultsCategory = (PreferenceCategory) findPreference("scan_results_category");

        mRefreshScanBtn = (ActionPreference) findPreference("refresh_scan");

        mRefreshScanBtn.setOnLickListener(v -> {
            mRefreshScanBtn.showLoading();
            mRefreshScanBtn.setEnabled(false);
            mSpWifiManager.startWifiScan();
        });
    }

    @Override
    protected int getResourceId() {
        return R.xml.scan_wifi_prefernces;
    }

    @Override
    protected void onReceivedMessage(Message msg) {
        switch (msg.what) {
            case WifiTracker.MSG_WIFI_SCAN_RESULT_AVAILABLE:
                onScanResultAvailable();
                break;
        }
    }

    private void onScanResultAvailable() {
        Context themedContext = getPreferenceManager().getContext();
        if (themedContext == null) {
            return;
        }
        mScanResultsCategory.removeAll();
        int i = 0;
        List<ScanResult> results = mSpWifiManager.getScanResults();
        for (ScanResult sr : results) {
            ActionPreference apPref = new ActionPreference(themedContext);

            apPref.setKey(AccessPointFragment.ACCESS_POINT_KEY_PREFIX + i++);
            apPref.setTitle(sr.SSID);
            apPref.setSummary(sr.BSSID);
            apPref.setSelectable(true);
            apPref.setFragment(AccessPointFragment.class.getName());

            SimplePreferenceDataStore dataStore = new SimplePreferenceDataStore();
            dataStore.putInt(AccessPointFragment.EXTRA_SCREEN_TYPE, AccessPointFragment.CONNECT_SCREEN);
            dataStore.putString(AccessPointFragment.EXTRA_BSSID, sr.BSSID);
            apPref.setPreferenceDataStore(dataStore);

            apPref.setOnLickListener(v -> {
                getFragmentManager().popBackStack();
            });

            mScanResultsCategory.addPreference(apPref);
        }
        mRefreshScanBtn.hideLoading();
        mRefreshScanBtn.setEnabled(true);
    }

    @Override
    protected int getListenerType() {
        return WIFI_LISTENER;
    }

    @Override
    protected String getName() {
        return "Scan Wifi";
    }
}
