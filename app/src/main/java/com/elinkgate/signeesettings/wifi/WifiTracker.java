package com.elinkgate.signeesettings.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;

import com.elinkgate.signee.SigneeManager;
import com.elinkgate.signee.wifi.SoftAccessPointCallback;
import com.elinkgate.signee.wifi.SupportWifiManager;
import com.elinkgate.signeesettings.Tracker;
import com.elinkgate.signeesettings.utils.ConnectivityThread;
import com.elinkgate.signeesettings.utils.Protocols;

public class WifiTracker extends Tracker {

    private static final int BASE = Protocols.WIFI;

    public static final int MSG_WIFI_STATE = BASE + 1;
    public static final int MSG_WIFI_CONNECTION = BASE + 2;
    public static final int MSG_WIFI_SCAN_RESULT_AVAILABLE = BASE + 3;
    public static final int MSG_WIFI_AP_STATE = BASE + 4;
    public static final int MSG_WIFI_AP_CLIENTS_CHANGED = BASE + 5;

    public static final int STATE_WIFI_CONNECTED = 1;
    public static final int STATE_WIFI_CONNECTING = 2;
    public static final int STATE_WIFI_DISCONNECTED = 3;
    public static final int STATE_WIFI_ERROR_AUTH = 4;

    private static final IntentFilter sFilter;

    static {
        sFilter = new IntentFilter();
        sFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        sFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        sFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        sFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
    }

    //private WifiManager mWifiManager;
    //private ConnectivityManager mConnectivityManager;

    private SupportWifiManager mSpWifiManager;

    private boolean mIsEnabled = false;
    private int mConnState = -1;

    private final BroadcastReceiver mWifiReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())) {
                onWifiStateChanged(intent);
            } else if (WifiManager.SUPPLICANT_STATE_CHANGED_ACTION.equals(intent.getAction())) {
                onWifiSupplicantStateChanged(intent);
            } else if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(intent.getAction())) {
                NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (info.isConnected()) {
                    mConnState = STATE_WIFI_CONNECTED;
                    sendMessage(MSG_WIFI_CONNECTION, mConnState);
                }
            } else if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent.getAction())) {
                sendMessage(MSG_WIFI_SCAN_RESULT_AVAILABLE);
            }
        }
    };

    private final SoftAccessPointCallback mSoftApCallback = new SoftAccessPointCallback() {

        @Override
        public void onStateChanged(int state, int failureCode) {
            Log.d("DEBUG", "Soft app state changed: " + state);
            sendMessage(MSG_WIFI_AP_STATE, state);
        }

        @Override
        public void onNumClientsChanged(int num) {
            Log.d("DEBUG", "Soft app num clients changed: " + num);
            sendMessage(MSG_WIFI_AP_CLIENTS_CHANGED, num);
        }
    };

    public WifiTracker(Context context) {
        super(context);
        //this.mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        //this.mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        this.mSpWifiManager = SigneeManager.getSupportWifiManager(context);
    }

    private void onWifiStateChanged(Intent intent) {
        int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
        mIsEnabled = state == WifiManager.WIFI_STATE_ENABLED;
        sendMessage(MSG_WIFI_STATE, mIsEnabled ? 1 : 0);
    }

    private void onWifiSupplicantStateChanged(Intent intent) {
        int err = intent.getIntExtra(WifiManager.EXTRA_SUPPLICANT_ERROR, -1);
        if (err == WifiManager.ERROR_AUTHENTICATING) {
            mConnState = STATE_WIFI_ERROR_AUTH;
            sendMessage(MSG_WIFI_CONNECTION, mConnState);
        } else {
            SupplicantState state = intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
            switch (state) {
                case SCANNING:
                case FOUR_WAY_HANDSHAKE:
                case GROUP_HANDSHAKE:
                case AUTHENTICATING:
                case ASSOCIATING:
                case ASSOCIATED:
                    mConnState = STATE_WIFI_CONNECTING;
                    break;
                case DISCONNECTED:
                    mConnState = STATE_WIFI_DISCONNECTED;
                    break;
                default:
                    return;
            }
            sendMessage(MSG_WIFI_CONNECTION, mConnState);
        }
    }

    @Override
    public void onStart() {
        if (mContextRef.get() != null) {
            mContextRef.get().registerReceiver(mWifiReceiver, sFilter, null,
                    new Handler(ConnectivityThread.getThreadLooper()));
        }
        mSpWifiManager.registerSoftApCallback(mSoftApCallback,
                new Handler(ConnectivityThread.getThreadLooper()));
    }

    @Override
    public void onStop() {
        if (mContextRef.get() != null) {
            mContextRef.get().unregisterReceiver(mWifiReceiver);
        }
        mSpWifiManager.unregisterSoftApCallback(mSoftApCallback);
    }

    @Override
    public void requestUpdate() {
        sendMessage(MSG_WIFI_STATE, mIsEnabled ? 1 : 0);
        sendMessage(MSG_WIFI_CONNECTION, mConnState);
    }
}
