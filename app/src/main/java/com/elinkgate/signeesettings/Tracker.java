package com.elinkgate.signeesettings;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;

public abstract class Tracker {

    protected WeakReference<Context> mContextRef;

    @Nullable
    private Handler mHandler;

    public Tracker(Context context) {
        this.mContextRef = new WeakReference<>(context);
    }

    public abstract void onStart();

    public abstract void onStop();

    public abstract void requestUpdate();

    public void setHandler(Handler handler) {
        this.mHandler = handler;
    }

    public void sendMessage(int what) {
        if (mHandler != null) {
            mHandler.sendEmptyMessage(what);
        }
    }

    public void sendMessage(int what, int arg1) {
        if (mHandler != null) {
            Message msg = Message.obtain();
            msg.what = what;
            msg.arg1 = arg1;
            mHandler.sendMessage(msg);
        }
    }

    public void sendMessage(int what, int arg1, Object obj) {
        if (mHandler != null) {
            Message msg = Message.obtain();
            msg.what = what;
            msg.arg1 = arg1;
            msg.obj = obj;
            mHandler.sendMessage(msg);
        }
    }
}
