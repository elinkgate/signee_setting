package com.elinkgate.signeesettings.leds;

import android.os.Bundle;

import androidx.preference.SwitchPreference;

import com.elinkgate.signee.BoardDefaults;
import com.elinkgate.signee.peripheral.LedController;
import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;

import java.util.function.Consumer;

public class LedsFragment extends BasePreferenceFragment {

    private SwitchPreference mLed1RedPref;
    private SwitchPreference mLed1GreenPref;

    private SwitchPreference mLed2RedPref;
    private SwitchPreference mLed2GreenPref;

    private SwitchPreference mLed3RedPref;
    private SwitchPreference mLed3GreenPref;

    private SwitchPreference mLedStatusRedPref;
    private SwitchPreference mLedStatusGreenPref;
    private SwitchPreference mLedStatusBluePref;

    private LedController mLedController;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        mLedController = new LedController(getContext());

        mLed1RedPref = (SwitchPreference) findPreference("led1_red");
        mLed1GreenPref = (SwitchPreference) findPreference("led1_green");

        mLed2RedPref = (SwitchPreference) findPreference("led2_red");
        mLed2GreenPref = (SwitchPreference) findPreference("led2_green");

        mLed3RedPref = (SwitchPreference) findPreference("led3_red");
        mLed3GreenPref = (SwitchPreference) findPreference("led3_green");

        mLedStatusRedPref = (SwitchPreference) findPreference("led_status_red");
        mLedStatusGreenPref = (SwitchPreference) findPreference("led_status_green");
        mLedStatusBluePref = (SwitchPreference) findPreference("led_status_blue");

        initUi();
    }

    private void initUi() {
        setupLedPref(mLed1RedPref, mLedController.isOn(BoardDefaults.LEDs.LED1_RED), (val) -> {
            if (val) {
                mLedController.turnOn(BoardDefaults.LEDs.LED1_RED);
            } else {
                mLedController.turnOff(BoardDefaults.LEDs.LED1_RED);
            }
        });

        setupLedPref(mLed1GreenPref, mLedController.isOn(BoardDefaults.LEDs.LED1_GREEN), (val) -> {
            if (val) {
                mLedController.turnOn(BoardDefaults.LEDs.LED1_GREEN);
            } else {
                mLedController.turnOff(BoardDefaults.LEDs.LED1_GREEN);
            }
        });

        setupLedPref(mLed2RedPref, mLedController.isOn(BoardDefaults.LEDs.LED2_RED), (val) -> {
            if (val) {
                mLedController.turnOn(BoardDefaults.LEDs.LED2_RED);
            } else {
                mLedController.turnOff(BoardDefaults.LEDs.LED2_RED);
            }
        });

        setupLedPref(mLed2GreenPref, mLedController.isOn(BoardDefaults.LEDs.LED2_GREEN), (val) -> {
            if (val) {
                mLedController.turnOn(BoardDefaults.LEDs.LED2_GREEN);
            } else {
                mLedController.turnOff(BoardDefaults.LEDs.LED2_GREEN);
            }
        });

        setupLedPref(mLed3RedPref, mLedController.isOn(BoardDefaults.LEDs.LED3_RED), (val) -> {
            if (val) {
                mLedController.turnOn(BoardDefaults.LEDs.LED3_RED);
            } else {
                mLedController.turnOff(BoardDefaults.LEDs.LED3_RED);
            }
        });

        setupLedPref(mLed3GreenPref, mLedController.isOn(BoardDefaults.LEDs.LED3_GREEN), (val) -> {
            if (val) {
                mLedController.turnOn(BoardDefaults.LEDs.LED3_GREEN);
            } else {
                mLedController.turnOff(BoardDefaults.LEDs.LED3_GREEN);
            }
        });

        setupLedPref(mLedStatusRedPref, mLedController.isOn(BoardDefaults.LEDs.LED_STATUS_RED), (val) -> {
            if (val) {
                mLedController.turnOn(BoardDefaults.LEDs.LED_STATUS_RED);
            } else {
                mLedController.turnOff(BoardDefaults.LEDs.LED_STATUS_RED);
            }
        });

        setupLedPref(mLedStatusGreenPref, mLedController.isOn(BoardDefaults.LEDs.LED_STATUS_GREEN), (val) -> {
            if (val) {
                mLedController.turnOn(BoardDefaults.LEDs.LED_STATUS_GREEN);
            } else {
                mLedController.turnOff(BoardDefaults.LEDs.LED_STATUS_GREEN);
            }
        });

        setupLedPref(mLedStatusBluePref, mLedController.isOn(BoardDefaults.LEDs.LED_STATUS_BLUE), (val) -> {
            if (val) {
                mLedController.turnOn(BoardDefaults.LEDs.LED_STATUS_BLUE);
            } else {
                mLedController.turnOff(BoardDefaults.LEDs.LED_STATUS_BLUE);
            }
        });
    }

    private void setupLedPref(SwitchPreference pref, boolean checked, Consumer<Boolean> valChangedCallback) {
        pref.setChecked(checked);

        pref.setOnPreferenceChangeListener((preference, newValue) -> {
            valChangedCallback.accept((boolean) newValue);
            return true;
        });
    }

    @Override
    protected int getResourceId() {
        return R.xml.leds_preferences;
    }

    @Override
    protected String getName() {
        return "LEDs";
    }
}
