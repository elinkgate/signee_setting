package com.elinkgate.signeesettings.ethernet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Handler;
import android.util.Log;

import com.elinkgate.signee.SigneeManager;
import com.elinkgate.signee.ethernet.InterfaceAvailabilityCallback;
import com.elinkgate.signee.ethernet.SupportEthernetManager;
import com.elinkgate.signeesettings.Tracker;
import com.elinkgate.signeesettings.utils.ConnectivityThread;
import com.elinkgate.signeesettings.utils.Protocols;

public class EthernetTracker extends Tracker {

    private static final int BASE = Protocols.ETHERNET;

    public static final int MSG_INTERFACE_AVAILABILITY_CHANGED = BASE + 1;
    public static final int MSG_ETHERNET_AVAILABLE = BASE + 2;
    public static final int MSG_ETHERNET_UNAVAILABLE = BASE + 3;

    private SupportEthernetManager mSpEthernetManager;
    private ConnectivityManager mConnectivityManager;

    public EthernetTracker(Context context) {
        super(context);
        this.mSpEthernetManager = SigneeManager.getSupportEthernetManager(context);
        this.mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    private final NetworkRequest mEthRequest = new NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build();

    private final ConnectivityManager.NetworkCallback mEthCallback = new ConnectivityManager.NetworkCallback() {

        @Override
        public void onAvailable(Network network) {
            sendMessage(MSG_ETHERNET_AVAILABLE);
        }

        @Override
        public void onLosing(Network network, int maxMsToLive) {
            sendMessage(MSG_ETHERNET_UNAVAILABLE);
        }

        @Override
        public void onLost(Network network) {
            sendMessage(MSG_ETHERNET_UNAVAILABLE);
        }

        @Override
        public void onUnavailable() {
            sendMessage(MSG_ETHERNET_UNAVAILABLE);
        }
    };

    private final InterfaceAvailabilityCallback mIFaceAvailabilityCallback = new InterfaceAvailabilityCallback() {
        @Override
        public void onAvailabilityChanged(String iFace, boolean isAvailable) {
            sendMessage(MSG_INTERFACE_AVAILABILITY_CHANGED, isAvailable ? 1 : 0, iFace);
        }
    };

    @Override
    public void onStart() {
        Log.d("DEBUG", "Register IFACE callback");
        mSpEthernetManager.registerInterfaceAvailabilityCallback(mIFaceAvailabilityCallback,
                new Handler(ConnectivityThread.getThreadLooper()));
        mConnectivityManager.registerNetworkCallback(mEthRequest, mEthCallback,
                new Handler(ConnectivityThread.getThreadLooper()));
    }

    @Override
    public void onStop() {
        Log.d("DEBUG", "Unregister IFACE callback");
        mSpEthernetManager.unregisterInterfaceAvailabilityCallback(mIFaceAvailabilityCallback);
        mConnectivityManager.unregisterNetworkCallback(mEthCallback);
    }

    @Override
    public void requestUpdate() {

    }
}
