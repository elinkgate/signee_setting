package com.elinkgate.signeesettings.ethernet;

import android.os.Bundle;

import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;
import com.elinkgate.signeesettings.utils.ActionPreference;
import com.elinkgate.signeesettings.utils.FeaturedEditPreference;

public class IpConfigFragment extends BasePreferenceFragment {

    private FeaturedEditPreference mIpAddressPref;
    private FeaturedEditPreference mSubnetMaskPref;
    private FeaturedEditPreference mGatewayPref;
    private FeaturedEditPreference mDnsPref;
    private ActionPreference mApplyButton;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        mIpAddressPref = (FeaturedEditPreference) findPreference("ip_address");
        mSubnetMaskPref = (FeaturedEditPreference) findPreference("subnet_mask");
        mGatewayPref = (FeaturedEditPreference) findPreference("gateway");
        mDnsPref = (FeaturedEditPreference) findPreference("dns");

        mApplyButton = (ActionPreference) findPreference("save_ip_config");
        mApplyButton.setOnLickListener(v -> getFragmentManager().popBackStack());

        mSubnetMaskPref.initText("255.255.255.0");
        mDnsPref.initText("8.8.8.8,8.8.4.4");
    }

    @Override
    protected int getResourceId() {
        return R.xml.ip_config_preferences;
    }

    @Override
    protected String getName() {
        return "IP configuration";
    }
}
