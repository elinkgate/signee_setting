package com.elinkgate.signeesettings.ethernet;

import android.os.Bundle;

import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;
import com.elinkgate.signeesettings.utils.ActionPreference;
import com.elinkgate.signeesettings.utils.FeaturedEditPreference;

public class ProxyConfigFragment extends BasePreferenceFragment {

    private FeaturedEditPreference mHostNamePref;
    private FeaturedEditPreference mPortPref;
    private FeaturedEditPreference mIgnoreHosts;
    private ActionPreference mSaveButton;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        mHostNamePref = (FeaturedEditPreference) findPreference("host");
        mPortPref = (FeaturedEditPreference) findPreference("port");
        mIgnoreHosts = (FeaturedEditPreference) findPreference("ignore_hosts");
        mSaveButton = (ActionPreference) findPreference("save_proxy_config");
        mSaveButton.setOnLickListener(v -> getFragmentManager().popBackStack());

        mHostNamePref.initText("5.141.244.52");
        mPortPref.initText("44087");
    }

    @Override
    protected int getResourceId() {
        return R.xml.proxy_preferences;
    }

    @Override
    protected String getName() {
        return "Proxy configuration";
    }
}
