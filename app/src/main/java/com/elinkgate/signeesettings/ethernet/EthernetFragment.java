package com.elinkgate.signeesettings.ethernet;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.ProxyInfo;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;

import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;

import com.elinkgate.signee.SigneeManager;
import com.elinkgate.signee.ethernet.EthernetConfiguration;
import com.elinkgate.signee.ethernet.IpAssignmentMethod;
import com.elinkgate.signee.ethernet.StaticIpConfiguration;
import com.elinkgate.signee.ethernet.SupportEthernetManager;
import com.elinkgate.signeesettings.BasePreferenceFragment;
import com.elinkgate.signeesettings.R;
import com.elinkgate.signeesettings.utils.ActionPreference;
import com.elinkgate.signeesettings.utils.FeaturedListPreference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EthernetFragment extends BasePreferenceFragment {

    private PreferenceCategory mEthInfoCategoryPref;
    private List<ActionPreference> mEthInfoPrefList = new ArrayList<>();
    private FeaturedListPreference mIpMethodListPref;
    private FeaturedListPreference mInterfaceListPref;
    private Preference mIpConfigPref;
    private Preference mProxyPref;
    private ActionPreference mResetInterfaceBtn;

    private SupportEthernetManager mSpEthernetManager;
    private ConnectivityManager mConnectivityManager;

    private List<String> mEthInterfaces;
    private String mSelectedInterface;
    private String mIpMethodValue = "0"; // dhcp

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        super.onCreatePreferences(savedInstanceState, rootKey);

        this.mSpEthernetManager = SigneeManager.getSupportEthernetManager(getActivity());
        this.mConnectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (mSpEthernetManager == null || !mSpEthernetManager.isAvailable()) {
            showInfoDialog("Interface unavailable", "Ethernet interface is not available");
            getFragmentManager().popBackStack();
            return;
        }

        addTracker(new EthernetTracker(getActivity()));

        mEthInterfaces = mSpEthernetManager.getAvailableInterfaces();
        mSelectedInterface = mEthInterfaces.get(0);

        mIpMethodListPref = (FeaturedListPreference) findPreference("ip_method");
        mIpConfigPref = findPreference("ip_config");
        mProxyPref = findPreference("proxy_config");
        mEthInfoCategoryPref = (PreferenceCategory) findPreference("eth_info_category");
        mInterfaceListPref = (FeaturedListPreference) findPreference("interface");
        mResetInterfaceBtn = (ActionPreference) findPreference("reset_interface");

        initUi();
    }

    private void initUi() {
        String[] entries = new String[mEthInterfaces.size()];
        String[] entryValue = new String[mEthInterfaces.size()];

        for (int i = 0; i < mEthInterfaces.size(); i++) {
            ActionPreference p = new ActionPreference(getActivity());
            p.setKey("eth_info_" + i);
            p.setSummary("Unknown");
            p.setTitle(mEthInterfaces.get(i));
            mEthInfoPrefList.add(p);
            mEthInfoCategoryPref.addPreference(p);

            entries[i] = mEthInterfaces.get(i);
            entryValue[i] = mEthInterfaces.get(i);
        }

        mInterfaceListPref.setEntries(entries);
        mInterfaceListPref.setEntryValues(entryValue);
        mInterfaceListPref.initValue(mEthInterfaces.get(0));

        mInterfaceListPref.setOnPreferenceChangeListener((preference, newValue) -> {
            mSelectedInterface = (String) newValue;
            return true;
        });
        mSelectedInterface = mInterfaceListPref.getValue();

        mIpMethodListPref.setOnPreferenceChangeListener((preference, newValue) -> {
            if ("1".equals(newValue)) {
                mIpConfigPref.setEnabled(true);
            } else {
                mIpConfigPref.setEnabled(false);
            }
            mIpMethodValue = (String) newValue;
            return true;
        });
        mIpMethodListPref.initValue(mIpMethodValue);

        mResetInterfaceBtn.setOnLickListener(v -> onResetInterface());

        clearNetworkInfo();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateConnectionInfo();
    }

    private void onResetInterface() {
        if (TextUtils.isEmpty(mSelectedInterface)) {
            showInfoDialog("Invalid configuration", "Select an interface");
            return;
        }

        if (!mSpEthernetManager.isAvailable(mSelectedInterface)) {
            return;
        }
        clearNetworkInfo();
        int ipMethod = getIpMethod();
        EthernetConfiguration ethConfig = new EthernetConfiguration(mSelectedInterface, ipMethod);
        if (ipMethod == IpAssignmentMethod.IP_METHOD_STATIC) {
            if (!staticConfig(ethConfig)) {
                showInfoDialog("Invalid configuration", "Invalid static configuration");
                return;
            }
        } else if (ipMethod == IpAssignmentMethod.IP_METHOD_UNASSIGNED) {
            return;
        }
        SharedPreferences sharedPreferences = getPreferenceManager().getSharedPreferences();
        boolean useProxy = sharedPreferences.getBoolean("use_proxy", false);
        if (useProxy && !configProxy(ethConfig)) {
            return;
        }

        if (!mSpEthernetManager.setConfiguration(mSelectedInterface, ethConfig)) {
            showInfoDialog("Invalid configuration", "Invalid ethernet configuration");
        }
    }

    private boolean staticConfig(EthernetConfiguration ethConfig) {
        SharedPreferences sharedPreferences = getPreferenceManager().getSharedPreferences();
        String ip = sharedPreferences.getString("ip_address", "");
        String subnet = sharedPreferences.getString("subnet_mask", "");
        String gateway = sharedPreferences.getString("gateway", "");
        if (TextUtils.isEmpty(ip) || TextUtils.isEmpty(subnet) || TextUtils.isEmpty(gateway)) {
            return false;
        }
        String dnsStr = sharedPreferences.getString("dns", "");
        StaticIpConfiguration staticIpConfig = new StaticIpConfiguration.Builder()
                .setIpAddress(ip)
                .setSubnetMask(subnet)
                .setGateway(gateway)
                .setDns(dnsStr)
                .build();
        ethConfig.setStaticIpConfig(staticIpConfig);
        return true;
    }

    private boolean configProxy(EthernetConfiguration ethConfig) {
        SharedPreferences sharedPreferences = getPreferenceManager().getSharedPreferences();
        String hostStr = sharedPreferences.getString("host", "");
        String portStr = sharedPreferences.getString("port", "");
        String ignoreHostsStr = sharedPreferences.getString("ignore_hosts", "");

        if (TextUtils.isEmpty(hostStr) && TextUtils.isEmpty(portStr)) {
            showInfoDialog("Invalid configuration", "Invalid proxy configuration");
            return false;
        }

        try {
            ProxyInfo proxyInfo;
            int port = Integer.parseInt(portStr);
            if (!TextUtils.isEmpty(ignoreHostsStr)) {
                List<String> ignoreHosts = Arrays.asList(ignoreHostsStr.split(","));
                proxyInfo = ProxyInfo.buildDirectProxy(hostStr, port, ignoreHosts);
            } else {
                proxyInfo = ProxyInfo.buildDirectProxy(hostStr, port);
            }
            ethConfig.setProxy(proxyInfo);
            return true;
        } catch (NumberFormatException e) {
            showInfoDialog("Invalid configuration", "Invalid port configuration");
            return false;
        }
    }

    @IpAssignmentMethod
    private int getIpMethod() {
        if (mIpMethodListPref.getValue().equals("0")) {
            return IpAssignmentMethod.IP_METHOD_DHCP;
        } else if (mIpMethodListPref.getValue().equals("1")) {
            return IpAssignmentMethod.IP_METHOD_STATIC;
        }
        return IpAssignmentMethod.IP_METHOD_UNASSIGNED;
    }

    @Override
    protected int getResourceId() {
        return R.xml.ethernet_preferences;
    }

    @Override
    protected int getListenerType() {
        return ETHERNET_LISTENER;
    }

    @Override
    protected void onReceivedMessage(Message msg) {
        switch (msg.what) {
            case EthernetTracker.MSG_ETHERNET_AVAILABLE:
                updateConnectionInfo();
                break;
            case EthernetTracker.MSG_ETHERNET_UNAVAILABLE:
                clearNetworkInfo();
                break;
            case EthernetTracker.MSG_INTERFACE_AVAILABILITY_CHANGED:
                if (mEthInterfaces.equals(msg.obj)) {
                    if (msg.arg1 == 0) {
                        setAllPreferenceEnabled(false);
                        setAllPreferenceEnabled(true);
                    } else {
                        updateConnectionInfo();
                    }
                }
                break;
            default:
                break;
        }
    }

    private void updateConnectionInfo() {
        final Network[] networks = mConnectivityManager.getAllNetworks();
        for (final Network network : networks) {
            NetworkInfo networkInfo = mConnectivityManager.getNetworkInfo(network);
            if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_ETHERNET) {
                updateConnectionInfo(network);
            }
        }
    }

    private void updateConnectionInfo(Network network) {
        LinkProperties linkProperties = mConnectivityManager.getLinkProperties(network);
        if (linkProperties != null) {

            int k = -1;
            for (int i = 0; i < mEthInterfaces.size(); i++) {
                if (mEthInterfaces.get(i).equals(linkProperties.getInterfaceName())) {
                    k = i;
                }
            }

            if (k >= 0) {
                String info = "IP address: " + linkProperties.getLinkAddresses().toString() +
                        "\nDNS address: " + linkProperties.getDnsServers().toString();
                mEthInfoPrefList.get(k).setSummary(info);
            }
        }
    }

    private void setAllPreferenceEnabled(boolean enabled) {
        for (ActionPreference p : mEthInfoPrefList) {
            p.setEnabled(enabled);
        }
        mIpMethodListPref.setEnabled(enabled);
        mProxyPref.setEnabled(enabled);
        if (enabled && mIpMethodValue.equals("0")) {
            mIpConfigPref.setEnabled(false);
        } else {
            mIpConfigPref.setEnabled(enabled);
        }
    }

    private void clearNetworkInfo() {
        for (ActionPreference p : mEthInfoPrefList) {
            p.setSummary("Unknown");
        }
    }

    @Override
    protected String getName() {
        return "Ethernet";
    }
}
