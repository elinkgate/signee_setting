package com.elinkgate.signeesettings.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;

import com.elinkgate.signeesettings.R;

public class ActionPreference extends Preference {

    private View.OnClickListener mOnLickListener;
    private boolean mOverrideOnClickListener = false;

    public ActionPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public ActionPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ActionPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActionPreference(Context context) {
        super(context);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        holder.itemView.setOnClickListener(view -> {
            if (mOnLickListener != null) {
                mOnLickListener.onClick(view);
            }
            if (!mOverrideOnClickListener) {
                performClick(view);
            }
        });
    }


    public void setOnLickListener(View.OnClickListener listener) {
        this.mOnLickListener = listener;
        this.mOverrideOnClickListener = false;
    }

    public void overrideOnClickListener(View.OnClickListener listener) {
        this.mOnLickListener = listener;
        this.mOverrideOnClickListener = true;
    }

    public void showLoading() {
        setWidgetLayoutResource(R.layout.loading);
    }

    public void hideLoading() {
        setWidgetLayoutResource(0);
    }
}
