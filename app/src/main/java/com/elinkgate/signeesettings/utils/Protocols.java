package com.elinkgate.signeesettings.utils;

public final class Protocols {

    private Protocols() {
        throw new RuntimeException("stub");
    }

    public static final int WIFI = 0x1000;
    public static final int BLUETOOTH = 0x1100;
    public static final int ETHERNET = 0x1200;
    public static final int TELEPHONY = 0x1300;
}
