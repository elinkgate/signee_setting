package com.elinkgate.signeesettings.utils;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.preference.PreferenceDataStore;

public class SimplePreferenceDataStore extends PreferenceDataStore {

    private Bundle mData = new Bundle();

    public SimplePreferenceDataStore() {
        super();
    }

    @Override
    public void putString(String key, @Nullable String value) {
        mData.putString(key, value);
    }

    @Override
    public void putBoolean(String key, boolean value) {
        mData.putBoolean(key, value);
    }

    @Nullable
    @Override
    public String getString(String key, @Nullable String defValue) {
        return mData.getString(key, defValue);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        return mData.getBoolean(key, defValue);
    }

    @Override
    public void putInt(String key, int value) {
        mData.putInt(key, value);
    }

    @Override
    public int getInt(String key, int defValue) {
        return mData.getInt(key, defValue);
    }

    @Override
    public void putLong(String key, long value) {
        mData.putLong(key, value);
    }

    @Override
    public void putFloat(String key, float value) {
        mData.putFloat(key, value);
    }

    @Override
    public long getLong(String key, long defValue) {
        return mData.getLong(key, defValue);
    }

    @Override
    public float getFloat(String key, float defValue) {
        return mData.getFloat(key, defValue);
    }
}
