package com.elinkgate.signeesettings.utils;

import android.content.Context;
import android.util.AttributeSet;

import androidx.preference.ListPreference;
import androidx.preference.PreferenceViewHolder;

public class FeaturedListPreference extends ListPreference {

    public FeaturedListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnPreferenceChangeListener(null);
    }

    public FeaturedListPreference(Context context) {
        super(context);
        setOnPreferenceChangeListener(null);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
    }

    @Override
    public void setOnPreferenceChangeListener(OnPreferenceChangeListener onPreferenceChangeListener) {
        OnPreferenceChangeListener listener = (preference, newValue) -> {
            setValue((String) newValue);
            setSummary(getEntry());
            if (onPreferenceChangeListener == null) {
                return true;
            }
            return onPreferenceChangeListener.onPreferenceChange(preference, newValue);
        };
        super.setOnPreferenceChangeListener(listener);
    }

    public void initValue(String value) {
        if (getOnPreferenceChangeListener() != null) {
            getOnPreferenceChangeListener().onPreferenceChange(this, value);
        }
    }
}
