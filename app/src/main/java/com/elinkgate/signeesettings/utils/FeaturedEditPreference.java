package com.elinkgate.signeesettings.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import androidx.preference.EditTextPreference;
import androidx.preference.PreferenceViewHolder;

public class FeaturedEditPreference extends EditTextPreference {

    private View.OnClickListener mOnLickListener;

    private boolean mEnableEdit = true;
    private boolean mHideSummary = false;
    private String mPlaceholder = "*";

    public FeaturedEditPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FeaturedEditPreference(Context context) {
        super(context);
        init();
    }

    private void init() {
        setOnPreferenceChangeListener(null);
        String txt = getText();

        if (TextUtils.isEmpty(txt)) {
            setSummary("N/A");
        } else {
            setSummary(txt);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onBindViewHolder(PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);
        holder.itemView.setOnClickListener(view -> {
            if (mOnLickListener != null) {
                mOnLickListener.onClick(view);
            }
            if (mEnableEdit) {
                performClick(view);
            }
        });
    }

    public void setEditTextEnabled(boolean enable) {
        mEnableEdit = enable;
    }

    @Override
    public void setOnPreferenceChangeListener(OnPreferenceChangeListener onPreferenceChangeListener) {
        final OnPreferenceChangeListener listener = (preference, newValue) -> {
            showValueAsSummary((String) newValue);
            if (onPreferenceChangeListener != null) {
                onPreferenceChangeListener.onPreferenceChange(preference, newValue);
            }
            return true;
        };
        super.setOnPreferenceChangeListener(listener);
    }

    public void setOnLickListener(View.OnClickListener listener) {
        this.mOnLickListener = listener;
    }

    public void initText(String text) {
        setText(text);
        showValueAsSummary(text);
    }

    public void setHideSummary(boolean hide) {
        this.mHideSummary = hide;
    }

    public void setPlaceholder(String placeholder) {
        if (placeholder == null) {
            this.mPlaceholder = "*";
            return;
        }
        this.mPlaceholder = placeholder;
    }

    private void showValueAsSummary(String text) {
        if (!mHideSummary) {
            setSummary(text);
        } else {
            setSummary(text.replaceAll(".", mPlaceholder));
        }
    }
}
