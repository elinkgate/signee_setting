package com.elinkgate.signeesettings.utils;

import android.os.HandlerThread;
import android.os.Looper;

public class ConnectivityThread extends HandlerThread {

    private static final class Singleton {
        private static final ConnectivityThread INSTANCE = create();
    }

    private ConnectivityThread() {
        super("connectivity_thread");
    }

    private static ConnectivityThread create() {
        ConnectivityThread thread = new ConnectivityThread();
        thread.start();
        return thread;
    }

    public static Looper getThreadLooper() {
        return Singleton.INSTANCE.getLooper();
    }
}
